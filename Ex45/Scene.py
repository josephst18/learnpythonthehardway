from sys import exit

class Scene(object):
    def enter(self):
        print("Override this scene")
        exit(1)
