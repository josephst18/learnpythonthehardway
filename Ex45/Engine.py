class Engine(object):
    def __init__(self, scene_map):
        self.scene_map = scene_map

    def play(self):
        next_scene = self.scene_map.opening_scene()
        last_scene = self.scene_map.next_scene("death")

        while next_scene != last_scene:
            current_scene = next_scene.enter()
            next_scene = self.scene_map.next_scene(current_scene)

        # take care of last scene (death)
        last_scene.enter()
