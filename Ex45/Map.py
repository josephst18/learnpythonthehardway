from Locations import *

class Map(object):
    scenes = {
        "beach": Beach(), 
        "house": House(), 
        "tennis": TennisCourts(), 
        "shops": GrandBoulevardShops(), 
        "death": Death()
    }

    def __init__(self, opening_scene):
        self.opening_scene = opening_scene

    def opening_scene(self):
        return next_scene(opening_scene)

    def next_scene(self, scene_name):
        val = Map.scenes.get(scene_name)
        return val