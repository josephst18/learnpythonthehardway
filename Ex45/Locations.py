from random import randint
from Scene import Scene

class Beach(Scene):
    def enter(self):
        print("You walk onto the beach and see a storm in the distance")
        print("Do you want to stay there or go home?")
        action = input("> ")

        if (action == "stay"):
            print("You stay at the beach and are killed by lightening")
            return "death"
        elif (action == "go home"):
            print("Wise choice. You go back home")
            return "house"
        else:
            print("I didn't understand that.")
            self.enter()

class House(Scene):
    def enter(self):
        print("There's no place like home, especially at the beach.")
        print("Where do you want to go?")
        action = input("> ")

        if (action == "beach"):
            return "beach"
        elif (action == "tennis courts"):
            return "tennis"
        elif (action == "grand boulevard shops"):
            return "shops"
        else:
            print("I didn't understand that.")
            self.enter()

class TennisCourts(Scene):
    def enter(self):
        print("Welcome to the tennis courts!")
        print("Would you like to play some tennis?")
        action = input("> ")

        if (action == "yes"):
            self.playTennis()
        elif (action == "no"):
            print("Then let's go back home.")
            return "home"
        else: 
            print("Didn't understand that")
            self.enter()
    def playTennis(self):
        print("You play a great round of tennis!")
        if (randint(0, 20) == 4):
            print ("Sadly, you suffer a fatal heart attack after finishing.")
            return "death"
        else:
            print("Want to continue playing?")
            action = input("> ")
            if (action == "yes"):
                playTennis()
            else:
                print("We're going home")


class GrandBoulevardShops(Scene):
    def enter(self):
        print("Welcome to the Shops at Grand Boulevard!")
        print("It's a great place to eat, shop, or see a movie.")
        print("Personally, I'd recommend the new Star Wars film.")
        print("Do you want to talk around any?")
        action = input("> ")

        if (action == "yes"):
            if (randint(0, 20) == 4):
                print("You were hit and killed while walking. Sorry!")
                return "death"
            else:
                print("You walk around some and go home.")
                return "home"
        else:
            print("Alright, we'll go back home")
            return "home"

class Death(Scene):
    print("All good things must come to and end.")
    print("You're dead, and with that the game is over.")
    print("Thanks for playing!")
    exit(0)